#!/usr/bin/env python3

#    This file is part of matrix-botty-boi.
#
#    matrix-ascii-bot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    matrix-ascii-bot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with matrix-ascii-bot.  If not, see <https://www.gnu.org/licenses/>.

import os
import configparser
import importlib.util
from matrix_client.client import MatrixClient
from threading import Event
from functools import partial

LOG_ERR = '<3>'
LOG_INFO = '<6>'


login_message = ('Hello, I am a bot. Type !help to find out how to'
                 ' use me and !help commands for a list of commands')

help_message = ('Type !help commands for a list of commands.\n'
                'Type !help <command> for help on a specific command')

config_message = ('Config file was created in {}, please fill out file '
                  'and start the bot again.')

plugin_message = ('Plugin folder was created in {}, please add plugins '
                  'to this directory, update the config file, '
                  'and start the bot again.')


matrix = None
config = None

def on_exception(e):
    matrix.logout()
    time.sleep(random.random() * 100)
    matrix.login(
        username=config['Connection']['MatrixUserName'],
        password=config['Connection']['Password'])
   

def help_command(room, incoming_event, plugins):
    body = incoming_event['content']['body']
    if len(body.split(' ')) == 1:
        room.send_text(help_message)
    elif body.split(' ')[1] == 'commands':
        print("building reply list")
        reply_list = []
        for plugin in plugins:
            reply_list.extend(plugin.get_commands())
        reply_list_numbered = []
        for index, element in enumerate(reply_list):
            reply_list_numbered.append(f'{index + 1}: {element}')
        reply = '\n'.join(reply_list_numbered)
        room.send_text(reply)
    elif len(body.split(' ')) > 1:
        help_dict_list = [plugin.get_command_help() for plugin in
                          plugins]
        for help_dict in help_dict_list:
            for key, value in help_dict.items():
                if ((key == body.split(' ')[1]) or
                    (key == body.split(' ')[1][1:]) or
                    (key == '!' + body.split(' ')[1])):
                    room.send_text(value)


def room_callback(room, incoming_event, matrix, plugins, config):
    body = incoming_event.get('content', {}).get('body')
    if not body:
        return
    if incoming_event['sender'] == config['Connection']['MatrixUserID']:
        return
    try:
        if incoming_event['content']['msgtype'] == 'm.text':
            if body.split(' ', 1)[0] == '!help':
                help_command(room, incoming_event, plugins)
    except Exception:
        return

    for plugin in plugins:
        plugin.room_callback(room, incoming_event, matrix)


def load_config(config_folder):

    config = configparser.ConfigParser()
    if not os.path.isdir(config_folder):
        os.makedirs(config_folder)
    config_file_path = os.path.join(config_folder, "config.ini")
    if not os.path.isfile(config_file_path):
        config['Connection'] = {'MatrixServerAddress': '',
                                'MatrixUserID': '',
                                'MatrixUserName': '',
                                'Password': '',
                                'RoomID': ''
                                }
        config['Plugins'] = {'#Example': 'Example.py',
                             '#PluginA': 'PluginA.py'
                             }
        with open(config_file_path, 'w') as config_file:
            config.write(config_file)
        print(config_message.format(config_file_path))
        raise FileNotFoundError(
            "config.ini was not found in the config folder")

    with open(config_file_path, 'r') as config_file:
        config.read_file(config_file)

    return config


def load_plugins(plugin_folder, plugins_config):

    if not os.path.isdir(plugin_folder):
        raise FileNotFoundError(
            "plugin_folder does not exist")

    plugins = []

    for plugin_name, filename, in plugins_config.items():
        try:
            spec = importlib.util.spec_from_file_location(
                plugin_name,
                os.path.join(plugin_folder, filename))
        except FileNotFoundError:
            print("{}, ")
        current_plugin = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(current_plugin)
        plugins.append(current_plugin)

    return plugins


def main():

    config_folder = os.path.join(os.environ['HOME'], ".config",
                                 "matrix-botty-boi")

    plugin_folder = os.path.join(os.environ['HOME'], ".local", "share",
                                 "matrix-botty-boi", "plugins")

    try:
        config = load_config(config_folder)
    except FileNotFoundError:
        if not os.path.isdir(plugin_folder):
            print(plugin_message.format(plugin_folder))
            os.makedirs(plugin_folder)
        exit(0)

    try:
        plugins = load_plugins(plugin_folder, config['Plugins'])
    except FileNotFoundError:
        print(plugin_message.format(plugin_folder))
        os.makedirs(plugin_folder)
        exit(0)

    matrix = MatrixClient(config['Connection']['MatrixServerAddress'],
                          user_id=config['Connection']['MatrixUserID']) 
    matrix.login(
        username=config['Connection']['MatrixUserName'],
        password=config['Connection']['Password'])

    room = matrix.join_room(config['Connection']['RoomID'])
    room.send_text(login_message)
    room.add_listener(partial(room_callback, matrix=matrix, plugins=plugins,
                              config=config))
    matrix.start_listener_thread(exception_handler=on_exception)
    print("Entering main loop")
    while True:
        Event().wait()


if __name__ == '__main__':
    main()
