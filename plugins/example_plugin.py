#    Example plugin for matrix-botty-boi


def get_commands():
    return ['!dummy', '!dummy2']


def get_command_help():
    help_dict = {}
    help_dict['!dummy'] = ('Type !dummy and nothing will happen!'
                           ' Isn\'t this useful?')
    help_dict['!dummy2'] = ('Type !dummy2 and still nothing will happen!'
                            ' This one will tell you about it though! '
                            ' Isn\'t this slightly more useful?')
    return help_dict


def room_callback(room, incoming_event, matrix):
    try:
        if incoming_event['content']['msgtype'] == 'm.text':
            body = incoming_event['content']['body']
            if body.split(' ', 1)[0] == '!dummy':
                pass
            elif body.split(' ', 1)[0] == '!dummy2':
                room.send_text("Nothing happened...2?")
    except Exception:
        return
