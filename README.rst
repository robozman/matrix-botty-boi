Matrix Botty Boi
================

A Matrix bot framework built using Python 3 and matrix-python-sdk

Configuration
-------------
*  The file ``config.ini.example`` contains example configuration. 
*  The application is hardcoded to look in ``~/.config/matrix-botty-boi`` for
   ``config.ini``.
*  Bot account credentials should be specified in the ``[Connection]`` section
   of this file
*  Plugin configuration should be specified in the ``[Plugins]`` section of
   this file

Plugin Requirements
-------------------
Each plugin file should contain the specified list of functions (see below)

See ``plugins/example_plugin.py`` for an example plugin

These functions have defined inputs and outputs that are required for full
functionaity.

``room_callback``
#################
The main function that controls the plugin's operations is ``room_callback``.
This function should contain the main logic for your plugin

This function takes three arguments:

*  room
    * this contains the matrix room object for the room the event occured
      in!help !dummy2
*  incoming_event
    * this contains the event that triggered the callback
*  matrix
    * this contains the matrix object assocated with the current
      connection (this is required for more complicated operations)
This function returns nothing

Example:

.. code-block:: python

    def room_callback(room, incoming_event, matrix):
        try:
            if incoming_event['content']['msgtype'] == 'm.text':
                body = incoming_event['content']['body']
                if body.split(' ', 1)[0] == '!dummy':
                    pass
                elif body.split(' ', 1)[0] == '!dummy2':
                    room.send_text("Nothing happened...2?")
        except Exception:
            return


``get_commands``
################

*  This function returns all the commands that the plugin can respond to
*  This command takes no arguments
*  This command should return an array of strings containing the available commands
Example:
    .. code-block:: python
        
        def get_commands():
            return ['!dummy', '!dummy2']

``get_command_help``
####################

*  This function returns a dictionary of commands to their help text
*  The dictionary should contain entries where the kes are all the commands
   returned by ``get_commands``
Example:

.. code-block:: python

    def get_command_help():
        help_dict = {}
        help_dict['!dummy'] = ('Type !dummy and nothing will happen!'
                               ' Isn\'t this useful?')
        help_dict['!dummy2'] = ('Type !dummy2 and still nothing will happen!'
                                ' This one will tell you about it though! '
                                ' Isn\'t this slightly more useful?')
        return help_dict
